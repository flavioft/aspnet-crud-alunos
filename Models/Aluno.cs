using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Alunos.Models
{
    public class Aluno
    {
        [Key]
        public int id { get; set; }

        [Required]
        public int turma_id { get; set; }

        [Required]
        public string nome { get; set; }

        [Required]
        public string matricula { get; set; }

        [ForeignKey("turma_id")]
        public virtual Turma turma { get; set; }
    }
}