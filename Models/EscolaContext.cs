using Microsoft.EntityFrameworkCore;

namespace Alunos.Models
{
    public class EscolaContext : DbContext
    {
        public DbSet<Aluno> Alunos { get; set; }
        public DbSet<Turma> Turmas { get; set; }

        public EscolaContext(DbContextOptions<EscolaContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Aluno>()
                    .HasOne(a => a.turma);
        }
    }
}